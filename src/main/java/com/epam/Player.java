package com.epam;

import com.epam.view.Console;

/**
 * This is the player.
 *
 * @author Rostyk Fedchuk
 * @version 1.8
 */
public final class Player {
    /**
     * Player is playing the game.
     *
     * @param args Windows command line arguments
     */
    public static void main(final String[] args) {
        new Console().play();
    }

    /**
     * Unused constructor.
     */
    private Player() {

    }
}
