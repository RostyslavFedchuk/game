package com.epam.model.record;

import com.epam.model.data.Property;
import com.epam.view.Color;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Represents records and names of the players.
 */
public class Record {
    /**
     * Represents all records and names of the players.
     */
    private Map<String, Double> records;

    /**
     * Count of top records.
     */
    public static final int COUNT_OF_RECORDS;

    /**
     * File record name.
     */
    private static final String RECORD_PATH;

    static {
        RECORD_PATH = Property.INSTANCE.getPropertyValue(
                "RECORD_PATH");
        COUNT_OF_RECORDS = Integer.valueOf(
                Property.INSTANCE.getPropertyValue(
                        "Count_of_top_records"));
    }

    /**
     * Initialize file name.
     *
     * @throws IOException Problems with file.
     */
    public Record() throws IOException {
        records = new LinkedHashMap<String, Double>();
        setRecords();
    }

    /**
     * Reads the records from the .txt file.
     *
     * @throws IOException Problems with the file.
     */
    public void setRecords() throws IOException {
        InputStream stream = getClass().getClassLoader().getResourceAsStream(RECORD_PATH);
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

        String line;
        while (!Objects.isNull((line = reader.readLine()))) {
            String[] players = line.split("-");
            if (players.length == 2) {
                if (records.containsKey(players[1])
                        && records.get(players[1]) > Double.valueOf(players[0])) {
                    records.put(players[1], Double.valueOf(players[0]));
                } else if (!records.containsKey(players[1])) {
                    records.put(players[1], Double.valueOf(players[0]));
                }
            }
        }
        reader.close();
        sortRecords();
    }

    /**
     * Sorts the records.
     */
    public void sortRecords() {
        records = records.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }

    /**
     * Gets the map with records and names.
     *
     * @return Records and names.
     */
    public Map<String, Double> getRecords() {
        return records;
    }

    /**
     * Saves the record to the file.
     *
     * @param time Time spent.
     */
    public void saveRecord(final double time) {
        System.out.println(Color.BLACK_BACKGROUND
                + Color.YELLOW + "Enter your name: ");
        String name = new Scanner(System.in).nextLine();
        File log = new File(RECORD_PATH);
        try {
            if (!log.exists()) {
                log.createNewFile();
            }
            PrintWriter printWriter = new PrintWriter(new FileWriter(log, true));
            printWriter.append(time + "-" + name + "\n");
            printWriter.close();
            System.out.println("Your record was saved!");
        } catch (IOException e) {
            System.out.println("Your record was NOT saved!!");
        }
    }

}
