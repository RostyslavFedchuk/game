package com.epam.model.record;

import com.epam.model.data.Property;

/**
 * Class converts time.
 */
public final class TimeConverters {
    /**
     * Nano.
     */
    private static final int NANO = 9;
    /**
     * The base of the number.
     */
    private static final int BASE = 10;
    /**
     * Count Of numbers after come.
     */
    private static final int ACCURACY;

    static {
        ACCURACY = Integer.valueOf(Property.INSTANCE.getPropertyValue(
                        "Count_of_numbers_after_come"));
    }

    /**
     * Unused.
     */
    private TimeConverters() {
    }

    /**
     * Convert time from nanoseconds to seconds with 2 number after coma.
     *
     * @param elapsedTime Time in nanoseconds.
     * @return Time in seconds.
     */
    public static double convertToSeconds(final long elapsedTime) {
        double seconds = elapsedTime / Math.pow(BASE, NANO);
        int hundreds = (int) (seconds * Math.pow(BASE, ACCURACY));
        return (double) hundreds / Math.pow(BASE, ACCURACY);
    }

}
