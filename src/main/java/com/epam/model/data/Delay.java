package com.epam.model.data;

import com.epam.exceptions.PropertyNotFoundException;

import java.util.Objects;

/**
 * Operates with game speed.
 */
public class Delay {
    /**
     * The speed of the main character.
     */
    private int value;
    /**
     * Decrease the delay of the player.
     */
    public static final Integer DECREASE;
    /**
     * Initiial delay value.
     */
    public static final Integer MAX;
    /**
     * Minimum dalay.
     */
    public static final Integer MIN;

    static {
        MAX = Integer.valueOf(Property.INSTANCE.getPropertyValue(
                        "Max_delay"));
        MIN = Integer.valueOf(Property.INSTANCE.getPropertyValue(
                        "Min_delay"));
        DECREASE = Integer.valueOf(Property.INSTANCE.getPropertyValue(
                "Decrease_delay_value"));
    }

    /**
     * Initial speed of the character.
     */
    public Delay() {
        value = MAX;
    }

    /**
     * Gets the delay value.
     *
     * @return Delay.
     */
    public int getValue() {
        return value;
    }

    /**
     * Sets the delay value.
     *
     * @param speed Delay.
     */
    public void setValue(final int speed) {
        value = speed;
    }

    /**
     * Gets the minimum delay.
     *
     * @return Minimum delay.
     */
    public int getMin() {
        return MIN;
    }

    /**
     * Gets the decrease value.
     *
     * @return Decrease value.
     */
    public int getDecrease() {
        return DECREASE;
    }

}
