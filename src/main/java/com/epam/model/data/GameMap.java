package com.epam.model.data;

import com.epam.exceptions.PropertyNotFoundException;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Possesses the map.
 *
 * @author Rostyk Fedchuk
 * @version 1.3
 */
public class GameMap {
    /**
     * The map of the game.
     */
    private List<List<Character>> map;

    /**
     * File map name.
     */
    private static final String FILE_MAP_NAME;

    static {
        FILE_MAP_NAME = Property.INSTANCE.getPropertyValue("FILE_MAP_NAME");
        if(Objects.isNull(FILE_MAP_NAME)){
            try {
                throw new PropertyNotFoundException();
            } catch (PropertyNotFoundException e) {
                e.showMessage();
            }
        }
    }

    /**
     * Reads the map from the .txt file.
     *
     * @throws IOException Problems with the file.
     */
    public void setMap() throws IOException {
        map = new LinkedList<>();
        InputStream stream = getClass().getClassLoader().getResourceAsStream(FILE_MAP_NAME);
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

        String line;
        while (!Objects.isNull((line = reader.readLine()))) {
            List<Character> row = new LinkedList<>();
            for (int i = 0; i < line.length(); i++) {
                row.add(line.charAt(i));
            }
            map.add(row);
        }
        reader.close();
    }

    /**
     * Call setMap() method.
     * Gets the created map of the game.
     *
     * @return The map.
     * @throws IOException Problems with the file.
     */
    public List<List<Character>> getMap() throws IOException {
        setMap();
        return map;
    }
}
