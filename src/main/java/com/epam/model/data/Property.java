package com.epam.model.data;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Helps to get properties from the file.
 */
public enum Property implements AutoCloseable {
    INSTANCE;

    private final Properties properties;

    Property() {
        properties = new Properties();
        close();
    }

    public String getPropertyValue(final String key) {
        return properties.getProperty(key);
    }

    @Override
    public void close(){
        try(InputStream input = getClass().getClassLoader().getResourceAsStream("config.properties")){
            properties.load(input);
        }catch (FileNotFoundException e) {
            System.out.println("Property file not found!");
        } catch (IOException e) {
            System.out.println("Problems with loading properties.");
        }
    }}
