package com.epam.model.data;

import com.epam.model.record.Record;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;
import java.util.List;

/**
 * Possesses all data of the game.
 *
 * @author Rostyk Fedchuk
 * @version 1.3
 */
public class Data {
    /**
     * The previous position(X,Y).
     */
    private List<Integer> positionWas;
    /**
     * The position(X,Y) of the '0'.
     */
    private List<Integer> positionIs;
    /**
     * The map of the game.
     */
    private List<List<Character>> map;
    /**
     * Record instance.
     */
    private Record record;
    /**
     * The position(X,Y) of the '1'.
     */
    private List<Integer> positionOne;
    /**
     * Represents the game is over or not.
     */
    private boolean gameOver;
    /**
     * Operate with the speed of the character.
     */
    private Delay delay;
    /**
     * Checks out if player pressed arrow keys.
     */
    private boolean move;

    /**
     * Sets map, positionWas and positionIs.
     *
     * @throws IOException Problems with the file.
     */
    public Data() throws IOException {
        record = new Record();
        gameOver = false;
        delay = new Delay();
        move = false;
        setMap(new GameMap().getMap());
        positionWas = new LinkedList<>();
        positionWas.add(-1);
        positionWas.add(-1);
        positionIs = new LinkedList<>();
        positionIs.add(0);
        positionIs.add(0);
    }

    /**
     * Sets move variable.
     *
     * @param set boolean type.
     */
    public void setMove(final boolean set) {
        move = set;
    }

    /**
     * Gets move variable.
     *
     * @return boolean type.
     */
    public boolean getMove() {
        return move;
    }

    /**
     * Gets the delay.
     *
     * @return Delay.
     */
    public int getDelay() {
        return delay.getValue();
    }

    /**
     * Sets the delay.
     *
     * @param speed Delay.
     */
    public void setDelay(final int speed) {
        delay.setValue(speed);
    }

    /**
     * Gets gameOver variable.
     *
     * @return True if game is over, other case - false.
     */
    public boolean getGameOver() {
        return gameOver;
    }

    /**
     * Sets gameOver variable.
     *
     * @param isOver Boolean type.
     */
    public void setGameOver(final boolean isOver) {
        this.gameOver = isOver;
    }

    /**
     * Sets previous position.
     *
     * @param was Previous X,Y coordinates.
     */
    public void setPositionWas(final List<Integer> was) {
        positionWas = was;
    }

    /**
     * Sets present position.
     *
     * @param is Present X,Y coordinates.
     */
    public void setPositionIs(final List<Integer> is) {
        positionIs = is;
    }

    /**
     * Sets the map.
     *
     * @param mapIn Map of the game.
     */
    public void setMap(final List<List<Character>> mapIn) {
        this.map = mapIn;
    }

    /**
     * Gets the map.
     *
     * @return The map.
     */
    public List<List<Character>> getMap() {
        return map;
    }

    /**
     * Sets the position of the '1' character.
     *
     * @param oneIs The position of the '1' character.
     */
    public void setPositionOne(final List<Integer> oneIs) {
        this.positionOne = oneIs;
    }

    /**
     * Gets the position of the '1' character.
     *
     * @return X, Y of the '1' character.
     */
    public List<Integer> getPositionOne() {
        return positionOne;
    }

    /**
     * Gets the present position of the '0' character.
     *
     * @return X, Y of the '0' character.
     */
    public List<Integer> getPositionIs() {
        return positionIs;
    }

    /**
     * Gets the previous position of the '0' character.
     *
     * @return X, Y of the '0' character.
     */
    public List<Integer> getPositionWas() {
        return positionWas;
    }

    /**
     * Gets the map with records and names.
     *
     * @return Records and names.
     */
    public Map<String, Double> getRecords() {
        return record.getRecords();
    }

    /**
     * Saves the record to the file.
     *
     * @param time Time spent.
     */
    public void saveRecord(final double time) {
        record.saveRecord(time);
    }

    /**
     * Gets the minimum delay.
     *
     * @return Minimum delay.
     */
    public int getMinDelay() {
        return delay.getMin();
    }

    /**
     * Gets the decrease value.
     *
     * @return Decrease value.
     */
    public int getDecreaseDelay() {
        return delay.getDecrease();
    }
}
