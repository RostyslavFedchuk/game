package com.epam.exceptions;

public class PropertyNotFoundException extends Throwable {

    public void showMessage(){
        System.out.println("Exception: property is not found!");
        System.exit(1);
    }
}
