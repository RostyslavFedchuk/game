package com.epam.controller;

import com.epam.model.data.Data;

import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Controls the process.
 *
 * @author Rostyk Fedchuk
 * @version 1.3
 */

public class ControllerImpl implements Controller {
    /**
     * All dara.
     */
    private Data data;

    public final Logger logger;

    /**
     * Initialize Model variable.
     */
    public ControllerImpl() {
        logger = LogManager.getLogger(ControllerImpl.class);
        try {
            data = new Data();
            data.setPositionOne(searchNumber('1'));
        } catch (FileNotFoundException e) {
            logger.trace("The system cannot "
                    + "find the path specified");
        } catch (IOException e) {
            logger.trace("Problems with map opening");
        } catch (ExceptionInInitializerError er) {
            logger.trace("Properties is not connected!");
        }
    }

    /**
     * Gets gameOver variable.
     *
     * @return True if game is over.
     */
    public boolean getGameOver() {
        return data.getGameOver();
    }

    /**
     * Gets the map.
     *
     * @return The map.
     */
    @Override
    public List<List<Character>> getMap() {
        try{
            return data.getMap();
        } catch (NullPointerException e){
            System.exit(1);
            return null;
        }
    }

    /**
     * Finds the given character.
     *
     * @param number Character to find.
     * @return Position(X, Y) of the found number.
     * Return empty list if it was not found.
     */
    public List<Integer> searchNumber(final char number) {
        List<Integer> result = new LinkedList<>();
        for (int i = 0; i < data.getMap().size(); i++) {
            int index = data.getMap().get(i).indexOf(number);
            if (index >= 0) {
                result.add(i);
                result.add(index);
            }
        }
        return result;
    }

    /**
     * Moves the main character - '0' automatically.
     */
    public void autoMove() {
        int x = data.getPositionIs().get(0);
        int y = data.getPositionIs().get(1);
        if (data.getPositionWas().get(0) != x
                && data.getPositionWas().get(1) != y + 1
                && data.getMap().get(x).get(y + 1) == ' ') {
            pressedArrowKeys(x, y + 1, x, y);
        } else if (data.getPositionWas().get(0) != x
                && data.getPositionWas().get(1) != y - 1
                && data.getMap().get(x).get(y - 1) == ' ') {
            pressedArrowKeys(x, y - 1, x, y);
        } else if (data.getPositionWas().get(0) != x + 1
                && data.getPositionWas().get(1) != y
                && data.getMap().get(x + 1).get(y) == ' ') {
            pressedArrowKeys(x + 1, y, x, y);
        } else if (data.getPositionWas().get(0) != x - 1
                && data.getPositionWas().get(1) != y
                && data.getMap().get(x - 1).get(y) == ' ') {
            pressedArrowKeys(x - 1, y, x, y);
        }
        data.setPositionIs(searchNumber('0'));
        if (isWin()) {
            data.setGameOver(true);
            return;
        }
    }

    /**
     * Keep moving your main character.
     */
    public void move() {
        int x = data.getPositionIs().get(0);
        int y = data.getPositionIs().get(1);
        if (data.getPositionWas().get(0) < 0) {
            if (data.getPositionWas().get(1) < y) {
                moveHelp(x, y + 1, x, y);
            } else {
                moveHelp(x, y - 1, x, y);
            }
        } else {
            if (data.getPositionWas().get(0) < x) {
                moveHelp(x + 1, y, x, y);
            } else {
                moveHelp(x - 1, y, x, y);
            }
        }
        data.setPositionIs(searchNumber('0'));
    }

    /**
     * Keep moving your main character, depending on the direction.
     *
     * @param xWill X coordinate to move on.
     * @param yWill Y coordinate to move on.
     * @param xIs   X coordinate that is now.
     * @param yIs   Y coordinate that is now.
     */
    public void moveHelp(final int xWill, final int yWill, final int xIs,
                         final int yIs) {
        if (!changeCharacter(xWill, yWill, '0')) {
            data.setGameOver(true);
            return;
        }
        changeCharacter(xIs, yIs, ' ');
        if (Math.abs(yWill - yIs) > 0) {
            data.setPositionWas(getLastMove(1, yIs));
        } else {
            data.setPositionWas(getLastMove(0, xIs));
        }
    }

    /**
     * Changes the character in (X,Y) to new character.
     *
     * @param x     X coordinate.
     * @param y     Y coordinate.
     * @param paste New character.
     * @return True if character is changed.
     */
    public boolean changeCharacter(final int x, final int y,
                                   final char paste) {
        if (data.getMap().get(x).get(y) == '1') {
            return false;
        }
        if (data.getMap().get(x).get(y) == '*' && paste == '0') {
            return false;
        } else {
            data.getMap().get(x).set(y, paste);
            return true;
        }
    }

    /**
     * This method is used for find out what was the last move.
     * If index == 0, the the character`s move was horizontally.
     * If index == 1, the the character`s move was vertically.
     *
     * @param index      Index to paste the coordinate.
     * @param coordinate X coordinate if index == 0,
     *                   Y - if index ==1.
     * @return One coordinate of the previous position and another is -1.
     * We use -1, so that means that we don`t need to check that coordinate,
     * because it stills the same.
     */
    public List<Integer> getLastMove(final int index, final int coordinate) {
        List<Integer> result = new LinkedList<>();
        for (int i = 0; i < 2; i++) {
            if (index == i) {
                result.add(coordinate);
            } else {
                result.add(-1);
            }
        }
        return result;
    }

    /**
     * Checks if the main character is nearby the '1'.
     *
     * @return Returns true if the main character is nearby the '1',
     * in other case - false.
     */
    public boolean isWin() {
        if (data.getPositionIs().get(0) == data.getPositionOne().get(0)) {
            if (Math.abs(data.getPositionIs().get(1)
                    - data.getPositionOne().get(1)) == 1) {
                return true;
            }
        }
        if (data.getPositionIs().get(1) == data.getPositionOne().get(1)) {
            if (Math.abs(data.getPositionIs().get(0)
                    - data.getPositionOne().get(0)) == 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if you use arrow keys.
     *
     * @return True if you use arrow keys.
     */
    @Override
    public boolean getMove() {
        return data.getMove();
    }

    /**
     * Sets the move variable.
     *
     * @param set boolean type.
     */
    @Override
    public void setMove(final boolean set) {
        data.setMove(set);
    }

    /**
     * Gets the Records.
     *
     * @return Map of records and names.
     */
    @Override
    public Map<String, Double> getRecords() {
        return data.getRecords();
    }

    /**
     * Moves the main character if given button pressed.
     *
     * @param e Key.
     */
    public void keyPressed(final KeyEvent e) {
        int x = data.getPositionIs().get(0);
        int y = data.getPositionIs().get(1);

        if (e.getKeyCode() == KeyEvent.VK_RIGHT
                || e.getKeyCode() == KeyEvent.VK_D) {
            pressedArrowKeys(x, y + 1, x, y);
        }
        if (e.getKeyCode() == KeyEvent.VK_LEFT
                || e.getKeyCode() == KeyEvent.VK_A) {
            pressedArrowKeys(x, y - 1, x, y);
        }
        if (e.getKeyCode() == KeyEvent.VK_DOWN
                || e.getKeyCode() == KeyEvent.VK_S) {
            pressedArrowKeys(x + 1, y, x, y);
        }
        if (e.getKeyCode() == KeyEvent.VK_UP
                || e.getKeyCode() == KeyEvent.VK_W) {
            pressedArrowKeys(x - 1, y, x, y);
        }
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            System.exit(0);
        }
        data.setPositionIs(searchNumber('0'));
    }

    /**
     * Gets the delay of the game process.
     *
     * @return Delay.
     */
    @Override
    public int getDelay() {
        return data.getDelay();
    }

    /**
     * Saves the record to the file.
     *
     * @param time Time spent.
     */
    @Override
    public void saveRecord(final double time) {
        data.saveRecord(time);
    }

    /**
     * Moves the character to the given direction.
     *
     * @param xWill X coordinate to move on.
     * @param yWill Y coordinate to move on.
     * @param xIs   X coordinate that is now.
     * @param yIs   Y coordinate that is now.
     */
    public void pressedArrowKeys(final int xWill, final int yWill,
                                 final int xIs, final int yIs) {
        if (xWill < data.getMap().size() && xWill >= 0) {
            if (yWill < data.getMap().get(xWill).size() && yWill >= 0) {
                if (data.getPositionWas().get(0) != xWill
                        && data.getPositionWas().get(1) != yWill) {
                    if (!changeCharacter(xWill, yWill, '0')) {
                        data.setGameOver(true);
                        return;
                    }
                    data.setMove(true);
                    if (data.getDelay() - data.getDecreaseDelay()
                            >= data.getMinDelay()) {
                        data.setDelay(data.getDelay() - data.getDecreaseDelay());
                    }
                    changeCharacter(xIs, yIs, ' ');
                    if (Math.abs(yWill - yIs) > 0) {
                        data.setPositionWas(getLastMove(1, yIs));
                    } else {
                        data.setPositionWas(getLastMove(0, xIs));
                    }
                }
            }
        }
    }
}
