package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import com.epam.model.record.Record;
import com.epam.model.record.TimeConverters;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The console of the game.
 * Shows the game.
 *
 * @author Rostyk Fedchuk
 * @version 1.2
 */
public class Console extends JFrame implements KeyListener {

    /**
     * Write logs.
     */
    private static Logger logger = LogManager.getLogger(Console.class);

    /**
     * This variable used to control the process.
     */
    private Controller controller;

    /**
     * Size of the Key listener frame.
     */
    private static final int SIZE = 10;

    /**
     * Initialize controller instance and gets the map.
     */
    public Console() {
        super("Key Listener");
        addKeyListener(this);
        JPanel pannel = new JPanel();
        add(pannel);
        setSize(SIZE, SIZE);
        setVisible(true);

        controller = new ControllerImpl();
    }

    /**
     * Prints out the map with the black background and green color of symbols.
     * '0' and '1' prints in another color
     */
    private void showMapColor() {
        String outputHelper = Color.BLACK_BACKGROUND
                + Color.YELLOW + "\t\t\t\t\t\t";
        String name = outputHelper + "\t\t" + "ZEROONE GAME\n";
        String line = "_________________________________"
                + "________________________________________";
        logger.trace(name + Color.BLACK_BACKGROUND
                + Color.GREEN + line + line);
        for (int i = 0; i < controller.getMap().size(); i++) {
            printLine(controller.getMap().get(i));
        }
        logger.trace(line + line);
        logger.trace(outputHelper + (("Move the zero to the One, "
                + "using arrow keys or W,S,A,D...").toUpperCase()));
        showRecords();
    }

    /**
     * Show the map in color with the printed message
     * in the middle of the map.
     *
     * @param message Message to print
     * @param time    time to show.
     * @throws IOException Problems with file.
     */
    private void getMessage(final String message,
                            final double time) throws IOException {
        int middleX = controller.getMap().size() / 2;
        int middleY = controller.getMap().get(0).size() / 2 - message.length() / 2;
        for (int i = 0; i < message.length(); i++) {
            controller.changeCharacter(middleX, middleY, message.charAt(i));
            middleY++;
        }
        String outputHelper = Color.BLACK_BACKGROUND
                + Color.YELLOW + "\t\t\t\t\t\t\t\t";
        String timeLine = outputHelper + "Time spent : " + time + " seconds\n";
        String line = "_________________________________"
                + "________________________________________";
        logger.trace(timeLine + Color.BLACK_BACKGROUND
                + Color.GREEN + line + line);
        for (int i = 0; i < controller.getMap().size(); i++) {
            printLine(controller.getMap().get(i));
        }
        logger.trace(line + line + Color.BLACK_BACKGROUND
                + Color.YELLOW);
        showRecords();
    }

    /**
     * Prints line in specific color.
     *
     * @param line Line of characters.
     */
    private void printLine(final List<Character> line) {
        String lineOutput = Color.BLACK_BACKGROUND + Color.GREEN + "[";
        for (int i = 0; i < line.size(); i++) {
            char symbol = line.get(i);
            switch (symbol) {
                case '0':
                    lineOutput += Color.BLACK_BACKGROUND
                            + Color.CYAN + symbol + " ";
                    break;
                case '1':
                    lineOutput += Color.BLACK_BACKGROUND
                            + Color.CYAN + symbol + " ";
                    break;
                case '*':
                    lineOutput += Color.BLACK_BACKGROUND
                            + Color.GREEN + symbol + " ";
                    break;
                case ' ':
                    lineOutput += Color.BLACK_BACKGROUND + symbol + " ";
                    break;
                default:
                    lineOutput += Color.WHITE_BACKGROUND
                            + Color.RED + symbol + " ";
            }
        }
        lineOutput += Color.BLACK_BACKGROUND + Color.GREEN + "]";
        logger.trace(lineOutput);
    }

    /**
     * Shows the top-5 records.
     *
     * @throws IOException Problems with file.
     */
    private void showRecords() {
        logger.trace(Color.BLACK_BACKGROUND + Color.YELLOW
                + "TOP " + Record.COUNT_OF_RECORDS + " RECORDS:");
        int count = 1;
        for (Map.Entry<String, Double> entry : controller.getRecords().entrySet()) {
            double time = entry.getValue();
            String name = entry.getKey();
            if (count <= Record.COUNT_OF_RECORDS) {
                logger.trace(Color.BLACK_BACKGROUND + Color.YELLOW
                        + count + ". " + name + " - " + time + " seconds");
            }
            count++;
        }
        logger.trace(Color.RESET);
    }

    /**
     * Clears the screen.
     * Used in command line.
     */
    private void cls() {
        try {
            new ProcessBuilder("cmd", "/c", "cls")
                    .inheritIO().start().waitFor();
        } catch (Exception e) {
            logger.trace(e.getCause());
        }
    }

    /**
     * Gaming process.
     */
    public void play() {
        try {
            long start = System.nanoTime();
            do {
                showMapColor();
                Thread.sleep(controller.getDelay());
                cls();
                if (!controller.getMove()) {
                    controller.move();
                }
                controller.setMove(false);
            } while (!controller.getGameOver());
            long elapsedTime = System.nanoTime() - start;
            double time = TimeConverters.convertToSeconds(elapsedTime);
            String log = "Time spent: " + time + " seconds";
            if (controller.isWin()) {
                getMessage("YOU~WON", time);
                controller.saveRecord(time);
                logger.info(log + " - Win");
            } else {
                getMessage("YOU~LOSE", time);
                logger.info(log + " - Lose");
            }
            logger.trace(Color.BLACK_BACKGROUND
                    + Color.YELLOW + "Press ESCAPE to quit...\n");
        } catch (InterruptedException ie) {
            logger.trace("Problems with sleep method");
        } catch (IOException e) {
            logger.trace("Cannot print the message!");
        }
    }

    /**
     * Unused.
     *
     * @param e Key.
     */
    @Override
    public void keyTyped(final KeyEvent e) {
    }

    /**
     * Call keyPressed method from controller.
     *
     * @param e Key.
     */
    @Override
    public void keyPressed(final KeyEvent e) {
        controller.keyPressed(e);
    }

    /**
     * Unused.
     *
     * @param e Key.
     */
    @Override
    public void keyReleased(final KeyEvent e) {

    }
}
