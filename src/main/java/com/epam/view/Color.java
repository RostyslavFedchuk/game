package com.epam.view;

/**
 * Colors and background colors of the text.
 *
 * @author Rostyk Fedchuk
 * @version 1.2
 */

public final class Color {

    /**
     * BLACK Background.
     */
    public static final String BLACK_BACKGROUND = "\033[40m";

    /**
     * BLACK Background.
     */
    public static final String RESET = "\033[0m";

    /**
     * High Intensity GREEN.
     */
    public static final String GREEN = "\u001B[32m";

    /**
     * Bold High Intensity RED.
     */
    public static final String RED = "\033[1;91m";

    /**
     * Bold High Intensity YELLOW.
     */
    public static final String YELLOW = "\033[1;93m";

    /**
     * Bold High Intensity CYAN.
     */
    public static final String CYAN = "\033[1;96m";

    /**
     * High Intensity WHITE background.
     */
    public static final String WHITE_BACKGROUND = "\033[0;107m";

    /**
     * Unused constructor.
     */
    private Color() {

    }
}
